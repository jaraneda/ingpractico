from django.contrib import admin

from models import Termo
from models import Mate
from models import Bombilla

# Register your models here.

admin.site.register(Termo)
admin.site.register(Mate)
admin.site.register(Bombilla)
