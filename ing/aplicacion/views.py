from django.shortcuts import render, render_to_response
from models import Termo
# Create your views here.

def listarTermo(request):
    if request.GET.get('id'):
        idTermo = request.GET.get('id')
        t = Termo.objects.filter(id=idTermo)
    else:
        t = Termo.objects.all()

    return render_to_response("termo.html", {"termos" : t })
