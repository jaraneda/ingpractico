from django.db import models

# Create your models here.

class Termo(models.Model):
    color = models.CharField(max_length=100)
    capacidad = models.IntegerField()
    marca = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Termo"
        verbose_name_plural = "Termos"

class Mate(models.Model):
    tipo = models.CharField(max_length=100)
    tamanio = models.IntegerField()

    class meta:
        verbose_name = "Mate"
        verbose_name_plural = "Mates"


class Bombilla(models.Model):
    material = models.CharField(max_length=100)
    tapado = models.BooleanField()

    class meta:
        verbose_name = "Bombilla"
        verbose_name_plural = "Bombillas"
#Metas
