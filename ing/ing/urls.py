from django.conf.urls import patterns, include, url

from django.contrib import admin

from aplicacion import views

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ing.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^termos/listar', views.listarTermo),
)
